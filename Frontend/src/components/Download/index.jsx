import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import DownloadService from "../../services/download.service";

const Download = () => {
    
  const [URL, setURL] = useState("");

  const navigate = useNavigate();

  const handleDownload = async (e) => {
    e.preventDefault();
    try {
      await DownloadService.download(URL).then(
        () => {
          navigate("/home");
          window.location.reload();
        },
        (error) => {
          console.log(error);
        }
      );
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <form onSubmit={handleDownload}>
        <h3>Download</h3>
        <input
          type="text"
          placeholder="file name"
          value={URL}
          onChange={(e) => setURL(e.target.value)}
        />
        <button className ="app_button" type="submit">Download</button>
      </form>
    </div>
  );
};

export default Download;