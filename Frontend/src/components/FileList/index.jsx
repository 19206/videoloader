import { useNavigate } from "react-router-dom";
import React, { useState } from "react";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import DownloadService from "../../services/download.service";
import LoadFileListService from "../../services/fileList.service";

const FileList = () => {
    
    const navigate = useNavigate();
    var files = JSON.parse(localStorage.getItem("files"));
    const [isLoad, setIsLoad] = useState(false);

    const handleDownload = async (name, e) => {
        e.preventDefault();
        try {
          await DownloadService.download(name).then(
            () => {
              navigate("/home");
              window.location.reload();
            },
            (error) => {
              console.log(error);
            }
          );
        } catch (err) {
          console.log(err);
        }
      };

    const handleLoadFileList = async (e) => {
      e.preventDefault();
      try {
        await LoadFileListService.loadFileList().then(
          () => {
            /*navigate("/home");*/
            //window.location.reload();
          },
          (error) => {
            console.log(error);
          }
        );
      } catch (err) {
        console.log(err);
      }
      setIsLoad(true);
    };

    return (
        <div>
            <button className ="app_button" onClick = {handleLoadFileList}>LoadFileList</button>
            {isLoad && 
            <ButtonGroup 
                orientation="vertical"
                color="primary"
                aria-label="vertical outlined primary button group"
            >
            {files.map(function(name, index){
                return <button className ="app_button" onClick = {(e) => handleDownload(name, e)} key={ index }>{name}</button>;
              })}
            </ButtonGroup>
            }   
        </div>
      );
    };
    export default FileList;