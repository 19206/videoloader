import { Grid, makeStyles, Container, Typography } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
}));

const Home = () => {
  const classes = useStyles();
  return (
    <Container maxWidth="sm" className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography className ="app_homePagetext" variant="h2" gutterBottom>
          </Typography>
          <Typography className ="app_homePagetext" variant="body1" gutterBottom>
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
}

export default Home;
