import React from "react";
import AuthService from "../../services/auth.service";
import { Grid, makeStyles, Container, TextField } from "@material-ui/core";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import validationSchema from "./validation";

const useStyles = makeStyles((theme) => ({
    root: {
      padding: theme.spacing(3),
    },
  }));

const Profile = () => {
  const currentUser = AuthService.getCurrentUser();
  const classes = useStyles();

  const {
    control,
    handleSubmit,
    formState: { errors },
    setError,
    reset,
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  if (currentUser) {
    console.log('Current user: ' + currentUser.username);
  } else {
    console.log('Current user not found');
  }
  console.log(currentUser);

  return (
    <Container maxWidth="xs" className={classes.root}>
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <Controller 
                name="username"
                control={control}
                defaultValue=""
                render={({ field }) => (
                    <TextField className ="app_text"
                    {...field}
                    error={Boolean(errors.username?.message)}
                    fullWidth={true}
                    label={currentUser.username}
                    variant="filled"
                    helperText={errors.username?.message}
                    />
                )}
                />
            </Grid>
            <Grid item xs={12}>
            <Controller
              name="email"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField className ="app_text"
                  {...field}
                  error={Boolean(errors.email?.message)}
                  fullWidth={true}
                  label={currentUser.email}
                  variant="filled"
                  helperText={errors.email?.message}
                />
              )}
            />
          </Grid>
        </Grid>
    </Container>
  );
};
export default Profile;