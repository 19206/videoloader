import axios from "axios";
const API_URL = "/api/downloadFile/";

const user = JSON.parse(localStorage.getItem("user"));

const download = (URL) => {
    return axios
      .get(API_URL + URL,{
          headers: {
            'Authorization': `Bearer  ${user.token}`
          },
          responseType: 'blob'
        })
      .then((response) => {
          console.log(response.data.fileName);
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', URL); //or any other extension
          document.body.appendChild(link);
          link.click();
      });
  };

  const downloadService = {
    download,
  };

  export default downloadService;