import axios from "axios";
const API_URL = "/api/fileList";

const user = JSON.parse(localStorage.getItem("user"));

const loadFileList = () => {
    return axios
      .get(API_URL,{
          headers: {
            'Authorization': `Bearer  ${user.token}`
          }
        })
      .then((response) => {
          localStorage.setItem("files", JSON.stringify(response.data.fileList));
      });
  };

  const LoadFileListService = {
    loadFileList,
  };

  export default LoadFileListService;