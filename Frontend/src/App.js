import { useState, useEffect } from "react";
import { Routes, Route, Link } from "react-router-dom";
import AuthService from "./services/auth.service";
import DownloadService from "./services/download.service";
import React from 'react'
import Login from "./components/Login/index";
import Signup from "./components/Signup/index";
import Profile from "./components/Profile/index";
import Home from "./components/Home/index";
import Download from "./components/Download/index";
import FileList from "./components/FileList/index"
import "bootstrap/dist/css/bootstrap.min.css";
import {useTheme} from './hooks/useTheme';
import { ThemeProvider } from "@material-ui/styles";
import "./App.css";
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  createTheme,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  rightToolbar: {
    flexGrow: 1,
  },
  title: {
    marginRight: theme.spacing(2),
  }
}));

const customTheme = createTheme({
  palette: {
    secondary: {
      main: "#F5BD1F",
      contrastText: "#4e005c"
    }
  }
});


function App() {
  const [currentUser, setCurrentUser] = useState(undefined);
  const {theme, setTheme} = useTheme();
  const classes = useStyles();

  useEffect(() => {
    const user = AuthService.getCurrentUser();

    if (user) {
      setCurrentUser(user);
    }
  }, []);

  const logOut = () => {
    AuthService.logout();
    setCurrentUser(false);
  };

  const handleLightThemeClick = () => {
    setTheme('light');
  }
  const handleDarkThemeClick = () => {
    setTheme('dark');
  }

  return (
    <div className="app_container">
       <div className={classes.root}>
        <ThemeProvider theme={customTheme}>
          <AppBar position="static"  color={"secondary"}>
           <Toolbar>
              <Typography variant="h6" className={classes.title}>
                 Real App
              </Typography>
              <div className={classes.rightToolbar}>
                <Button className ="app_button" component={Link} to="/home">
                  Home
                </Button>
                <Button className ="app_button" onClick={() => handleLightThemeClick()}> Light </Button>
                <Button className ="app_button" onClick={() => handleDarkThemeClick()}> Dark </Button>
              </div>
              {currentUser ? (
                <>
                <Button className ="app_button" component={Link} to="/profile">
                   Profile
                </Button>
                <Button className ="app_button" component={Link} to="/loadFileList">
                  FileList
                </Button>
                <Button className ="app_button" component={Link} to="/download">
                    Download
                  </Button>
                  <Button className ="app_button" onClick={logOut} component={Link} to="/home">
                    Log out
                  </Button>
                </>
              ) : (
                <>
                  <Button className ="app_button" component={Link} to="/signin">
                    Login
                  </Button>
                  <Button className ="app_button" component={Link} to="/signup">
                    Registration
                  </Button>
                </>
        )}
        </Toolbar>
        </AppBar>
        </ThemeProvider>
        <div className="container mt-3">
          <Routes>
            <Route path="/home" element={<Home />} />
            <Route path="/signin" element={<Login />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/download" element={<Download />} />
            <Route path="/loadFileList" element={<FileList />} />
          </Routes>
          </div>
        </div>
    </div>
  );
}

export default App; 