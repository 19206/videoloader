package com.example.demo.service;

import com.example.demo.configs.properties.FileStorageProperties;
import com.example.demo.exception.FileStorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileListService {
    private final Path fileStorageLocation;

    @Autowired
    public FileListService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }
    public List<String> loadFileList() {
        List<String> fileNameList = new ArrayList<String>();
        try {
            File uploadDir = new File(String.valueOf(this.fileStorageLocation));
            File[] files = uploadDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                fileNameList.add(files[i].getName());
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return fileNameList;
    }
}
