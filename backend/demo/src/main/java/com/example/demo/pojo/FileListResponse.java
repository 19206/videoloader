package com.example.demo.pojo;

import java.util.List;

public class FileListResponse {
    private List<String> fileList;

    public FileListResponse(List<String> fileList) {
        this.fileList = fileList;
    }

    public List<String> getFileList() {
        return fileList;
    }

    public void setFileList(List<String> fileList) {
        this.fileList = fileList;
    }
}
