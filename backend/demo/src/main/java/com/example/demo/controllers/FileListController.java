package com.example.demo.controllers;

import com.example.demo.pojo.FileListResponse;
import com.example.demo.pojo.MessageResponse;
import com.example.demo.service.FileListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@RestController
public class FileListController {
    private static final Logger logger = LoggerFactory.getLogger(FileListController.class);
    @Autowired
    private FileListService fileListService;

    @GetMapping("/api/fileList")
    public ResponseEntity<?> loadFileList() {
        // Load file as Resource
        List<String> fileList = fileListService.loadFileList();
        return ResponseEntity.ok(new FileListResponse(fileList));
    }

}
