package com.example.demo.pojo;

public class DownloadRequest {

    private String downloadURL;

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }
}
