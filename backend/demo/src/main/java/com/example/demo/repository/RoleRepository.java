package com.example.demo.repository;

import com.example.demo.models.ERole;
import com.example.demo.models.Role;
import com.fasterxml.jackson.databind.ext.OptionalHandlerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.cdi.JpaRepositoryExtension;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);

}
